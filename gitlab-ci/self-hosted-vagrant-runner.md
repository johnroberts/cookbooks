# Summary
To self-host a GitLab runner VM, clone this repo: https://gitlab.com/johnroberts/vm-gitlab-runner.git and follow the instructions in its README.md.

It's useful to add that repo as a git submodule to a project that needs to execute CI pipelines, and can't run on GitLab for cost/security/performance reasons.