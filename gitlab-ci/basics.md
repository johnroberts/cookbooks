# Core Concepts
*Job*: unit of execution within a pipeline. Each job has a `script:` section, and belongs to a stage

*Stage*: a grouping of jobs that execute at a given position within a pipeline. Ex: build, test, deploy

*Runners*: hosts that execute jobs in the pipeline. https://gitlab.com hosts shared free-tier runners (400 minutes per month); self-hosting is also possible and unlimited.

*Executors*: pre-configured types of runners for various scenarios. Common ones are `shell` and `docker`.
- For security (and peformance) reasons, avoid using the docker runner on a host that runs other containers or contains sensitive data/functionality. This is because using it requires mounting the Docker socket into the container, effectively giving it the ability to control Docker and the underlying host.

*Artifacts*: files/directories created by a job; useful for packaging, compilation, rendering templatized files, generating files for a deployment, etc.

*Environment*: GitLab's representation of a deployment environment (e.g. dev, staging, production). There is a set of environment variables per environment.

*`.gitlab-ci.yml`*: file in project root that defines the pipeline(s) to execute

*Pipeline*: end-to-end execution unit that executes jobs stage-by-stage.

# Resources
- Quick start: https://docs.gitlab.com/ee/ci/quick_start/
- Gitlab CI reference: https://docs.gitlab.com/ee/ci/yaml/index.html