# Keyboard shortcuts
- Ctrl + Shift + C: wrap selection in a cloze deletion

# Anki feature usage
- Use tags rather than subdecks (tags are spaced-separated, use_underscores_in_tags). Tag at card creation time.
- One deck per retrieval context (exam, interview, etc.)

# Mobile v. desktop
- Use mobile primarily for reviews
- Create cards on desktop