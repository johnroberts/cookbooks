TODO: complete this

# Schema-building card template
Don't just memorize by rote; create schemas while learning topics with this card pattern

## Question

## Answer

## Additonal context (optional)

# Card design patterns
- Precise, focused questions; simple answers
- Learn first; then create flashcards from what you're learned
- Three specific cards > one vague card (decompose a concept to smallest elements). Ex: instead of “What are Newton’s Three Laws of Motion?”, have “What is Newton’s First Law of Motion?”, “What is Newton’s Third Law of Motion?”, “Which law states F=ma?”
- 

# Card design anti-patterns
- Making cards too long
- Statement/definitional card: word/phrase on front, definition on back
- Use *italics* and **bold** in the question. Ex: What *type* of immunity acts against **previously** encountered agents?

# Recall/reinforcement tricks
- Redundancy; covering the same topic from multiple angles/lenses
- Use clozes; visual and text-based
- 