# Anki benefits
- Encode information into long term memory at will, rather than by chance
- Use as learning/schema-building tool (question, answer, context/linkage to existing schema)
- Exploit spaced repetition to remember stuff effectively
- Study only what needs studying; eliminate time waste of re-studying due to forgetting
- Turn downtime into study time -- Anki exists on mobile and desktop, habituate to Anki as default dopamine craving