# Resources
- Good + bad card design examples: https://media.publit.io/file/13-FDD.pdf
- Practical, thorough, thoughtful intro: https://leananki.com/how-to-use-anki-tutorial/
- How to write good prompts: https://andymatuschak.org/prompts/
- Ankify your life: https://abouttolearn.substack.com/p/anki-fy-your-life
- Detailed example of schema-building with Anki: http://augmentingcognition.com/ltm.html
- Card design for schema-building: https://borretti.me/article/effective-spaced-repetition