# Summary
Depending how you use it, Anki can help with:
- Rote memorization
- Learning, building schema, expanding mental model library

# Anti-patterns
- Memorizing before learning; to create schema, move back and forth between Anki and learning source material, adding cards to cover the whole topic area
- Skipping reviews; the more reviews are skipped, the more natural forgetting occurs
- Using shared decks; part of the schema-building is creating the cards. Exceptions are for memorization-heavy topics with high-quality shared decks (i.e. med students)