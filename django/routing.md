# Django routing basics
`django.urls.path`: maps a URL path to a view/page
`django.urls.include`: import a URLConf Python module from elsewhere in the project. Useful for constructing URLs in a modular way for sub-applications within a project.

