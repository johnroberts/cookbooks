# Concepts
- Project: collection of sites and metadata/config
- Site: an individual site/app
- Migrations: build or apply database changes, syncing model changes to the database. Part of the ORM.
- Models: Python files that define a site's data and its relationships. Part of the ORM.
- Apps: middleware-ish bits used by a given site that influence the DB schema. Defined by `project/site/settings.py` -> `INSTALLED_APPS`. Includes things like the admin webapp, auth and session management, a messaging framework for interacting with the user, etc. "Batteries-included" approach aims for defaults fitting a CRUD webapp.

# Frequently-used Django commands
## tool: django-admin (mainly Django-wide commands)
Create project skeleton (including `/admin` app): `django-admin create-project myproject`

## manage.py (mainly per-site commands)
`manage.py` is created by `django-admin` commands, and has useful utilities for common development or ops tasks.

### Common dev tasks
Start a dev web server (in a container on port 8000, with hot-reload for most files): `manage.py runserver 0.0.0.0:8000`
Create app/site boilerplate within a project: `manage.py startapp app-name`
Django-specific linting: `manage.py check` 

### Django admin webapp
Create the superuser: `manage.py createsuperuser`
Change superuser password: `manage.py changepassword` 

### Shells
Get a django shell (imports for the project and site): `manage.py shell`
Get a DB shell for the configured ORM: `manage.py dbshell`

### Database
See [./db.md](./db.md)