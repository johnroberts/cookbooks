# Django DB/ORM basics
## Common commands
- Create superuser for Django built-in auth (in `/admin` route): `python manage.py createsuperuser`

## Defining DB schema
- Create a class inherting from `django.db.models` (usually in `project/site/model.py`)
- Define fields with attribute in the class, where each attribute is a `django.db.models.SomethingField`. There are field types for common data types (strings, integers, whatnot).
    - Name these `SomethingField` attributes descriptively; they are used elsewhere in your Django code, and as column names in the DB.
    - Optionally, pass the first positional argument to a `SomethingField` as a human-readable name; this is used in a few places within Django, and doubles as documentation.
    - Different field types have various required and optional parameters: maximum length, default values, etc.
- Define a string-ified representation of the class: `def __str__(self) -> str`. This is useful when using the Django admin site, and also in interactive shells (`manage.py shell`).
- Define a relationship with another field, for example: `question = models.ForeignKey(Question, on_delete=models.CASCADE)`

## Handling DB migrations (sync models with DB)
Migrations are stored as `.py` files on disk (under `project/site/migrations`). The typical workflow:
1. Update the model code
2. Create migrations for updated model code: `manage.py makemigrations`
3. Run migrations: `manage.py migrate`

- View DB migrations (check DB against current model definitions): `manage.py showmigrations`
- Create DB migration `.py` files (under `project/site/migrations`): `manage.py makemigrations`
- View the SQL to be executed for a DB migration: `manage.py sqlmigrate app_name migration_name`
- Run DB migration (apply changes calculated from `makemigrations`): `manage.py migrate`
- Create DB/apply DB schema changes: `python manage.py migrate`
