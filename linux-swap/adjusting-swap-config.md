# Create a swapfile
## Common sizes
First create a zero-filled file:
1GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=1048576`
2GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=2097152`
4GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=4194304`
8GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=8388608`
16GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=16777216`
32GB: `sudo dd if=/dev/zero of=/swapfile bs=1024 count=33554432`

## Custom size
```
size = bs * count

block_size=1024 # 1KB (in bytes)
block_count=1048576   # 1GB with 1024 byte block size

sudo dd if=/dev/zero of=/swapfile bs=$block_size count=block_count
```

## Tighten swapfile permissions
```
sudo chown root:root /swapfile
sudo chmod 600 /swapfile
```

## Turn zero-filled file into a swapfile
`sudo mkswap /swapfile`

## Activate swapfile immediately
`sudo swapon /swapfile`

# Use swapfile across reboots (/etc/fstab)
Add this line to `/etc/fstab`:
```
/swapfile    none    swap    sw    0    0
```

Fields explanation:
- **File system**: Swapfile path
- **Mount point**: doesn't apply to swapfiles, so "none"
- **Type**: “swap”
- **Options**: "sw". At boot time `swapon -a` (start all devices marked as swap) will be called from a boot script. This option tells Linux to treat this entry as a swap resource that should come under the control of that `swapon -a` command.
- **Dump**: doesn't apply to swapfiles, so "0"
- **Pass**: doesn't apply to swapfiles, so "0"

# Adjusting swappiness value
`sudo sysctl vm.swappiness=n`

Values are 0-100:
- Default value is 60
- Higher values = more aggressive use of swap space
- Lower values = less aggressive use of swap space
- Per-host tuning is useful

# Disabling swap
```
# Disable a swap partition or file
sudo swapoff /dev/nvme0n1p2

# Disable swap
sudo swapoff -a
```