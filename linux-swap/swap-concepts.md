# What is swap space?
Swap space/swap is a section a system's storage (SSD/HDD) that functions as a part of virtual memory. Swap holds memory pages that are temporarily inactive.

# Why have swap space?
Swap space is used when the operating system decides that it needs physical memory for active processes, but there's not enough physical memory available. It's also used during when suspending or hibernating a system.

When this happens, inactive pages from the physical memory are then moved into the swap space, freeing up that physical memory for other uses.

Having adequate swap space also prevents the out-of-memory (OOM) killer from forceably ending processes; this is especially important in use cases where availability is important.

# Constraints
The access time for swap is slower than physical RAM, depending on the speed of the SSD/HDD. Swap isn't a replacement for physical RAM.

# Swap partition vs. swap file
Swap space can be:
- A partition (better performance, on older kernels)
- A swap file (easier to resize, near-identical-performance to partitions in modern kernels)
- A combo of partition/file

# Swap space management
Depending on the system, various components interact to manage swap:
- systemd (on systems that use it)
- Entries in `/etc/fstab`
- "Swappiness" value (`systemctl vm.swappiness`)

# Resources
- Solid, practical summary: https://www.baeldung.com/linux/swap-file-partition
- Swappiness explained: https://www.howtogeek.com/449691/what-is-swapiness-on-linux-and-how-to-change-it/
- Arch swap explanation/help: https://wiki.archlinux.org/title/Swap
- Ubuntu swap explanation/help: https://help.ubuntu.com/community/SwapFaq
