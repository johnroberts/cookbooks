# Check current swap config
`swapon --show`

or...

`free -h` (look in Swap: section)

# Check swappiness value
`cat /proc/sys/vm/swappiness`