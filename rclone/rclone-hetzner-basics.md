# Prereqs
- Buy a Hetzner storage box: https://www.hetzner.com/_ray/pow
- Turn on SSH support for the Hetzner storage box
- Create a dir in storage box for encrypted content

# Config Example
## Basic (unencrypted)
`~/.config/rclone/rclone.conf`:
```
[storagebox]
type = sftp
host = uXXXXX.your-storagebox.de
user = uXXXXX
port = 23
pass = <obscured-password>
```

## Crypt (encrypt a basic config)
First create a directory on the remote that will hold encrypted data (`crypt` in this example).

In `~/.config/rclone/rclone.conf` that already has a working remote, use the `crypt` provider to wrap it:
```
[storagebox-crypt]
type = crypt
remote = storagebox:crypt
filename_encryption = standard
directory_name_encryption = true
password = <obscured-password>
```

This can also be done interactively (`rclone config` -> New remote -> crypt -> etc.)

# Obscure a password
```sh
# Prepend with a space to avoid shell history
 rclone obscure your-password
```

# Mount a remote
```sh
# Create a mountpoint
mkdir ~/storagebox-mount

# Mount an rclone remote to the mountpoint
rclone mount storagebox:path ~/storagebox-mount
```