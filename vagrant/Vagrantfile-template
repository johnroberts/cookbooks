Vagrant.configure("2") do |config|
    ##### BASE BOX #####
    config.vm.box = "vagrant-cloud-account/some-box"
    # config.vm.box = "/path/to/local/base.box"

    ##### NETWORKING #####
    config.vm.hostname = 'your-hostname'
    config.vm.network "private_network", type: "dhcp"
    # config.vm.network "private_network", ip: "192.168.50.4"

    ##### VIRTUALBOX CUSTOMIZATION ######
    config.vm.provider "virtualbox" do |v|
        v.memory = 2048 # define RAM
        v.gui = true    # for machines with a desktop environment

        # Bidirectional clipboard
        v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    end

    ##### PROVISIONING ######
    # Ansible-based provisioning
    config.vm.provision "ansible_local" do |ansible|
        ansible.install = true              # this is the default
        ansible.playbook = "playbook.yml"   # playbook.yml in project dir
    end

    # Workaround for ansible_local provisioner to use the project's ansible.cfg on Windows hosts
    config.vm.synced_folder ".", "/vagrant",  mount_options: ["dmode=775,fmode=755"]

    # Shell-based provisioning, inline script
    config.vm.provision "shell",
        inline: "echo Hello, World"

    # Shell-based provisioning, external script
    config.vm.provision "shell", path: "scripts/script.sh" # execute scripts/script.sh

    ##### SHARED DIRECTORIES ######
    # Vagrant default: project directory mounted at /vagrant in the VM 
    
    # Mount <project dir>/src to /srv/website in the VM 
    config.vm.synced_folder "src/", "/srv/website"
end