# Explanation
On some platforms/versions, Virtualbox creates a host-only network adapter that includes a DHCP server. Vagrant expects to manage Virtualbox's DHCP server configuration, and gets upset with Virtualbox's default host-only network that includes a DHCP server.

# Error message
With a `Vagrantfile` networking config like:
```
config.vm.network "private_network", type: "dhcp"
```

Running `vagrant up` produces this error:

```
A host only network interface you're attempting to configure via DHCP
already has a conflicting host only adapter with DHCP enabled. The
DHCP on this adapter is incompatible with the DHCP settings. Two
host only network interfaces are not allowed to overlap, and each
host only network interface can have only one DHCP server. Please
reconfigure your host only network or remove the virtual machine
using the other host only network.
```

# Fix
List Virtualbox's network adapters: `VBoxManage list dhcpservers`

Look in the output for `NetworkName` (such as `HostInterfaceNetworking-VirtualBox Host-Only Ethernet Adapter`)

Then delete the conflicing DHCP server: `VBoxManage dhcpserver remove --netname 'HostInterfaceNetworking-VirtualBox Host-Only Ethernet Adapter'`

More info at https://github.com/hashicorp/vagrant/issues/3083