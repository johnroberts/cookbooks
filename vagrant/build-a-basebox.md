# Outline
- Download ISO
- Create VM in Virtualbox + install OS
    - large, dynamically expanding disk
    - 512MB RAM for CLI, 2048 for GUI
    - Disable unneeded controllers (USB, audio)
- Reboot
- Install Virtualbox guest additions, reboot
- Install updates
- Install OpenSSH server
- Ensure SSH port is open in firewall
- Set up `vagrant/vagrant` user: default password, passwordless sudo, default Vagrant public key
- Install Ansible and dependencies (so `ansible_local` provisioner can work later from `Vagrantfile`)
- Shut down VM
- Package with `vagrant box`

# Detailed Steps (Debian/Ubuntu-derived distros)
- Download official ISO, use this as the base
- Name VM as `os-base`
- Boot order hard drive first
- Manually add live ISO as IDE device
- Disable unneeded controllers (USB, audio)
- 175GB VMDK dynamically expanding virtual drive
- English/US keyboard layout
- Minimal installation
- Update packages during installation
- Guided use entire disk defaults
- Set timezone to UK time (for UTC)
- `vagrant/vagrant` user created, configured for auto GUI login
- Machine name/hostname: `os-base`

## Install Virtualbox Guest Additions notes
Do these after initial install + reboot.

- Need to install prereqs before guest additions (to build and load the virtualbox kernel modules): `sudo apt install build-essential dkms linux-headers-generic`
- Then insert guest additions CD, mount it, and install it by executing `VBoxLinuxAdditions.run` from the mounted guest additions ISO image as root

## Install updates
Apply all available updates

## Vagrant prereqs notes
- `vagrant/vagrant` user exists (happens during manual install)
- `vagrant` user is capable of passwordless sudo
`sudo visudo`, then add and save this content:
```
# Allow vagrant user passwordless sudo
vagrant ALL=(ALL) NOPASSWD: ALL
```
- Install and start openssh server: `sudo apt install -y openssh-server`
- Disable DNS lookups: `UseDNS no` in `/etc/ssh/sshd_config`
- `vagrant` user trusts insecure Vagrant public key in `~/.ssh/authorized_keys`:
```bash
rm -Rf /home/vagrant/.ssh
mkdir /home/vagrant/.ssh
wget -O /home/vagrant/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub
chown -R vagrant:vagrant /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
```

# Clean the box
Create and execute `basebox-cleanup.sh`:
```bash
#!/bin/bash

##### Summary #####
# Purpose: prepare a Linux machine for packaging as a base box
# Execute this just before packaging the box with vagrant
# Adapted from Vagrant Cloud box: aaronvonawesome/linux-mint-21-cinnamon

##### Apt #####
printf "STEP: Clean up APT\n"
sudo apt-get clean -y
sudo apt-get autoclean -y
sudo apt autoremove -y

##### Empty Space #####
printf "STEP: Zero out all the empty space\n"
sudo dd if=/dev/zero of=/tmp/EMPTY bs=1M
sudo rm -rf /tmp/*

##### Shell History #####
printf "STEP: Delete shell history\n"
unset HISTFILE
sudo rm -f /root/.bash_history
rm -f /home/vagrant/.bash_history
```

# Package the box
Once prereqs are done shut down the guest, then run this on the host with `vagrant` and `virtualbox`:
```bash
mkdir ~/vagrant-boxes
cd ~/vagrant-boxes
vagrant package --base os-base --output os-base.box
vagrant box add os-base.box --name personal-boxes/OS
```

After adding the box to Vagrant, should be able to use it in a `Vagrantfile` and/or upload it to Vagrant Cloud.

# Resources
- Adapting this to Kubuntu: https://oracle-base.com/articles/vm/create-a-vagrant-base-box-virtualbox
- https://app.vagrantup.com/aaronvonawesome/boxes/linux-mint-21-cinnamon