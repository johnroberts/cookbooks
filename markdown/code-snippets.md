# Code inline
To get this: `code`

Type this:
```
`code`
```

# Code block (language-specific syntax highlighting)
Specify language after 3 backticks to open code block

```sh
whoami
pwd

`
```sh
whoami
pwd
```
`

# Code block (language-agonstic syntax highlighting)
```
a = 1
b = 2
c = 3
```

`
```
a = 1
b = 2
c = 3
```
`