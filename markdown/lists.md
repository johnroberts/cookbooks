# Ordered list
1. Item 1
2. Item 2
3. Item 3

```md
1. Item 1
2. Item 2
3. Item 3
```

# Unordered list
- An item
- Another item
- Some other item

```md
- An item
- Another item
- Some other item
```
