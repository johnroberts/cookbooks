# Inline
[DuckDuckGo](https://duckduckgo.com): `[DuckDuckGo](https://duckduckgo.com)`

# Relative link in a git repo
- [A project script](scripts/do-something.sh)
- [PasteOps Guide](docs/pasteops-guide.md)

```md
- [A project script](scripts/do-something.sh)
- [PasteOps Guide](docs/pasteops-guide.md)
```