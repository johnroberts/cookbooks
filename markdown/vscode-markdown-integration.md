VSCode has a bunch of nice features for working with Markdown, see https://code.visualstudio.com/Docs/languages/markdown

# Keyboard Shortcuts
`Control + Shift + O`: jump to header

`Control + Shift + V`: preview rendered version in new tab

`Control + K, then V`: preview rendered version side-by-side

`Control + Space`: auto-complete relative link paths (IntelliSense)

`##` within a markdown to browse markdown headings from all markdown docs in current workspace

`Shift + Alt + right/left`: expand/shrink smart selection (headers, lists, code blocks, block quotes, etc.)

`F2`: rename a markdown header, and links to it (same as the rename symbol shortcut)

# Mouse Shortcuts
Drag and drop a file from explorer view into a markdown document to create a link to it