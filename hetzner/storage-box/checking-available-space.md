# Via web interface
https://robot.hetzner.com/storage

![Available space through web interface](img/storage-box-available-space-web.png)

# Via script/CLI
SFP: `echo "df -h"  | sftp username@username.your-storagebox.de`

SSH: `ssh -p23 uXXXXX@uXXXXX.your-storagebox.de df -h`