# What Is Hetzner Storage Box?
## Summary
Super cheap RAID-backed storage for bulk data/backups. Automation and manual use friendly. Accessed via network-drive-style protocols.

https://docs.hetzner.com/robot/storage-box/general

## Supported protocols
- FTP/SFTP
- SFTP/SCP, rsync
- WebDAV/HTTPS
- CIFS/SMB

## Supported software
- Any software speaking the supported protocols
- restic
- rclone
- borgbackup
- Network drive mapping

## Cost
$3-4 USD per terabyte per month for 1TB (larger plans are cheaper per TB). There are 1TB, 5TB, 10TB, and 20TB plans.

## Reliability
- Best-effort service: no guarantees/contractual protections against data loss
- "RAID-based configuration which can withstand several drive failures. Therefore, there is a relatively small chance of data being lost. The data is not mirrored onto other servers."
- "Checksums for the individual data blocks are used to detect and correct bit errors"

## Limits
https://www.hetzner.com/storage/storage-box
- **Data size**: Can store up to your storage plan's limit; storing more requires upgrading to the next plan
- **Bandwidth/network**: unlimited
- **Simulatenous connections**: 10
- **Snapshots**: 10+ (varies per plan)
- **Automated snapshots**: 10+ (varies per plan)
- **Subaccounts**: 100
- **Datacenter location**: Finland or Germany