# List snapshots, view snapshot contents, view a file in a snapshot
(Assumes you have (specified a repository)[specifying-a-repository.md])

```
# List snapshots
restic snapshots

# Select a snapshot, copy its id into a variable
SNAPSHOT_ID=2a104e20

# List contents of the selected snapshot
restic ls $SNAPSHOT_ID

# Print a file from the repository to STDOUT
restic dump $SNAPSHOT_ID /path/to/printable/file
```

# Mount a repository
(Assumes you have (specified a repository)[specifying-a-repository.md])

```
# Create a mountpoint
mkdir ~/restic-mountpoint
export RESTIC_MOUNTPOINT=~/restic-mountpoint

# Mount $RESTIC_REPOSITORY
restic mount ~/restic-mountpoint
```
