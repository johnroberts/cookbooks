# Restic Core Concepts
- Repository: an encrypted file structure containing backup data and metadata
- Authentication: symmetric encryption via password (can have multiple per repository)
- Snapshot: a set of files backed up from a single host at a specific time (multiple snapshots per repository)
- Hosts: the host where a snapshot is sourced from (can be one or multiple per repository)

# Supported Targets
- Local filesystem
- SFTP/SSH
- Dedicated restic server
- Various cloud storages
- Rclone wrapper (which supports a huge range of cloud providers)