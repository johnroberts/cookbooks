# Set the backup repository
```
export RESTIC_REPOSITORY=your-repository
```

# Authenticate via a password file
```
# Prepend space to avoid password in shell history 
 echo randomly-generated-password > .restic-pw

# Tighten permissions
chown root:root # or whatever is right for your environment
chmod 400 .restic-pw

export RESTIC_PASSWORD_FILE=.restic-pw
```

# Authenticate via environment variable
```
# Set the repository dir/string
export $RESTIC_REPOSITORY=existingrepository

# Prepend space to avoid password in shell history
 export RESTIC_PASSWORD=your-password-here

restic snapshots
```