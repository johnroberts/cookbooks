# Guides
Three-part LinuxBabe guide:
1. https://www.linuxbabe.com/security/a-practical-guide-to-gpg-part-1-generate-your-keypair
2. https://www.linuxbabe.com/security/gpg-guide-public-key-management
3. https://www.linuxbabe.com/security/gpg-encrypt-and-decrypt-message

General-purpose DigitalOcean guide: https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages

Details of subkeys, with some nice graphics: https://rgoulter.com/blog/posts/programming/2022-06-10-a-visual-explanation-of-gpg-subkeys.html

# Cheatsheets
- https://devhints.io/gnupg
- http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/