# Workaround: key generation hang/timeout
By default, `gpg` prompts to protect generated private keys with a passphrase with a `pinentry` program. This prompt can cause a hang when using a desktop environment/graphical window manager. Work around this by specifying a CLI `pinentry` program.

In `~/.gnupg/gpg-agent.conf`:
```
pinentry-program /usr/bin/pinentry-curses
```

Refresh gpg-agent's config after changing `gpg-agent.conf`: `gpg-connect-agent reloadagent /bye`

More on this issue: https://superuser.com/questions/520980/how-to-force-gpg-to-use-console-mode-pinentry-to-prompt-for-passwords/521027#521027