# Encrypting
Import keys to your keyring before encrypting to them.

## Encrypt a file to a public key
`gpg --encrypt -r <RECIPIENT> secret.txt`

- By default, the encrypted file will be at `secret.txt.gpg`
- Specify `<RECIPIENT>` by key ID, email, or name (or a substring)

## Encrypt a file to a public key (specify output file)
`gpg --encrypt --output secret.txt.gpg -r <RECIPIENT> secret.txt`

- Specify `<RECIPIENT>` by key ID, email, or name (or a substring)

## Encrypt a file to multiple public keys
`gpg --encrypt --output secret.txt.gpg -r <RECIPIENT1> -r <RECIPIENT2> secret.txt`

- Specify `<RECIPIENT>` by key ID, email, or name (or a substring)

## Encrypt a file using a symmetric key
`gpg --symmetric secret.txt`

You'll be prompted for the encryption passphrase.

# Decrypting
## Decrypting an encrypted file
`gpg --decrypt --output secret.txt secret.txt.gpg`

- By convention, gpg-encrypted files have a `.gpg` extension.
- If the file was encrypted using a symmetric key, you'll be prompted for the passphrase.