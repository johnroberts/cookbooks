# Keyserver purpose
Keyservers allow for searching, uploading, and downloading public keys for use with `gpg`. https://keys.openpgp.org is the default for many `gpg` installations (and a widely-used keyserver).

# Searching for a public key
`gpg --search-key somebody@example.com`

You can search by email address, key ID, or key fingerprint

# Import a key from a keyserver
`gpg --receive-keys <KEY IDS>`

# Upload keys to a keyserver
`gpg --send-keys <key or keys>`

# Refresh keys already in your keyring from a keyserver
`gpg --refresh-keys`

# Override default keyserver config
`gpg --keyserver <URL> ...`