# Basics
- GPG uses public and private (secret) keys. There are separate commands for working with public vs. private keys.
- GPG provides both signing and encryption capabilities
- GPG can encrypt data using symmetric encryption rather than public-key encryption
- apt
- Public keys can be exchanged directly, or through a key server (such as https://keys.openpgp.org/)
- Private keys should be protected with a passphrase, and stored in a secure location (such as paper backups, password manager, or dedicated servers-management server)

# Use Cases
- GPG is used extensively in Linux-land
- Apt uses GPG for repository and package signing
- Can sign git commits with GPG keys
- Can use GPG to encrypt a git remote (`git-remote-gcrypt`)