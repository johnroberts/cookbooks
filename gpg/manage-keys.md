# Generate keypair
## Interactive
```shell
# Super-quick generation (prompt for passphrase only)
gpg --quick-generate-key someone@example.com default default 3y

# Quick generation (less detailed prompts)
gpg --gen-key

# Full generation (more detailed prompts)
gpg --full-generate-key
```

After generating:
- Back up the keypair in a secure, private location
- If this is a long-term or production/important key, create a revocation certificate

## Quick-generate signing key (no encryption key)
```shell
# Quick-gen an RSA 4096-bit signing key for a user; doesn't gen encryption subkey 
gpg --quick-generate-key --default-new-key-algo rsa4096 someone@example.com
```

## Create revocation certificate
```shell
# Generate revocation cert
gpg --output ~/revoke.crt --gen-revoke "existinguser@example.com"

# Protect recovation cert
chmod 600 ~/revoke.crt
```

Upon generating a keypair, you should generate and store a revocation certificate. This provides a way of invalidating your key (in case of a security breach, losing access to your private key, etc.). Store this revocation certificate in a secure, private location.

# Export/back up
## Back up a single identity
```shell
# Back up public key to public.gpg
gpg --export --export-options backup --output public.gpg existinguser@example.com

# Back up private key to private.gpg
gpg --export-secret-keys --export-options backup --output private.gpg existinguser@example.com
```

## Back up public keys from keyring to public_keys.gpg
`gpg --export --export-options backup --output public_keys.gpg`

## Back up trust database
`gpg --export-ownertrust > trust.gpg`

This file (`~/.gnupg/trustdb.gpg`) contains trust information about which public keys.

# Import/restore
## Import keys
```shell
gpg --import key.gpg # could be individual public/private keys, or a keyring
```

This is the same command to import any pubkey from a file.

## Import trust database
`gpg --import-ownertrust trust.gpg`

# View/list keys
## List public keys
`gpg --list-public-keys`

## List secret keys
`gpg --list-secret-keys`

# Fingerprint/check keys
You can generate a fingerprint (checksum) from a public key, then ask the public key's holder to give you their fingerprint (over email, phone...some out-of-band mechanism). This provides a convenient way to verify you're importing the proper key.

## Fingerprint your own key (from keyring)
`gpg --fingerprint`

## Fingerprint a public key from a file
`gpg --show-keys file.pub`

# Import public keys
## From a file
`gpg --import public.key`

## From a keyserver
`gpg --receive-keys <KEY IDS>`

# Sign a public key with your private key
First import the key you want to trust into your gpg keyring, then trust it with: `gpg --sign-key <KEY ID>`

This:
- Tells your local `gpg` installation to trust this key
- Gives you the ability to export the signed key and provide it to the original keyholder; 

# Export keys
## In GPG native (binary) format
`gpg --output key.gpg --export <KEY ID>`

## In text (ASCII) format
`gpg --output key.asc  --armor --export <KEY ID>`

By convention `.asc` files are used for text keys.

# Trust a key
```shell
gpg --edit-key <KEY ID>

# In the interactive prompt:
gpg> trust
gpg> save
gpg> quit
```

# Delete keys
For full keypairs delete the private key first, then the public key.

## Private key
`gpg --delete-secret-key "someone@example.com"`

(Or by key ID)

## Public key
`gpg --delete-key "someone@example.com"`

(Or by key ID)