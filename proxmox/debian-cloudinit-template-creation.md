# Create the VM
```sh
# Get a raw disk image of Debian stable
wget https://cloud.debian.org/images/cloud/bookworm/20240507-1740/debian-12-genericcloud-amd64-20240507-1740.qcow2 -O debian-stable-cloud.img

# First install libguestfs-tools package, then...
# Add qemu-guest-agent into the disk image
virt-customize -a debian-stable-cloud.img --install qemu-guest-agent

# Create a new VM
qm create 8000 --memory 2048 --core 2 --name debian-base --net0 virtio,bridge=vmbr0 --ostype l26

# Import the downloaded disk image to the VM. Note the disk name for the next step
qm disk import 8000 debian-stable-cloud.img local-lvm

# Attach the disk to the VM (as a virtio SCSI device)
qm set 8000 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-8000-disk-0

# Add a cloud-init drive (as an CDROM device, aka IDE2)
qm set 8000 --ide2 local-lvm:cloudinit

# Make the cloud-init drive bootable, set the VM to boot from the Debian disk image
qm set 8000 --boot c --bootdisk scsi0

# Add a serial console display to the VM (used by many cloud-init images)
qm set 8000 --serial0 socket --vga serial0

# Enable the guest agent (optional, but useful)
qm set 8000 --agent enabled=1

# Create a template from the VM
qm template 8000

# Create a linked clone from the template
qm clone 8000 8100 --name new-debian-vm

# In proxmox web UI, customize the hardware and cloud-init settings:
#   - cloud-init tab: default username and SSH pubkey (for ansible)
#   - Options tab: enable qemu guest agent (if not enabled from previous cmd)
#   - Hardware tab: resize the VMs disk
```