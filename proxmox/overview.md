# What is ProxMox?
It's a converged platform for VMs and containers based on Debian, KVM/Qemu, and LXC. It's super flexible, supporting all kinds of storage, virtualization, and networking setups. There's a nice web UI for common tasks, plus a CLI and API.

