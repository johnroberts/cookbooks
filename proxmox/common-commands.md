```sh
# List VMs
qm list

# Create a VM (with ID 8000 and sensible defaults)
qm create 8000 --memory 2048 --core 2 --name debian-base --net0 virtio,bridge=vmbr0
```