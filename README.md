# Author
[John Roberts](https://jroberts.io)

# Purpose
Capture reusable learnings from my projects: concept summaries, copy-pasteable snippets or templates, and useful links.

Inspired by https://github.com/DevDungeon/Cookbook and [PasteOps](https://dl.acm.org/doi/fullHtml/10.1145/3194653.3197520)

Overlaps a bit with my [dotfiles](https://gitlab.com/johnroberts/dotfiles).