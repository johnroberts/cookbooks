[IPv6 cheatsheet (OneMarcFifty)](https://github.com/onemarcfifty/cheat-sheets/blob/main/networking/ipv6.md)
- Address format
- IP assignment methods (SLAAC, DHCPv6)
- Scopes
- Special addresses
- Subnetting
- Multicast and common addresses
- ICMPv6 common message types
- Common Wireshark filters
- Unicast, multicast, anycast

[Neighbor Discovery Protocol (NDP) summary](https://blog.apnic.net/2019/10/18/how-to-ipv6-neighbor-discovery/)
- NS: neighbor solicitation (sent by a node to determine/validate a neighbor's link-layer address, or perform duplicate address detection)
- NA: response to an NS (can also by sent unsolicited to announce a link-layer address change)
- RS: router solicitation (request at interface up time to request router info immediately)
- RA: router advertisement (routers advertising their presence and config params, periodically or in response to a RS message)

[ARP vs NDP](https://superuser.com/a/969839)
- ARP lets IPv4 hosts determine link-layer addresses, NDP lets IPv6 hosts determine link-layer addresses
- NDP enables automatic address config (SLAAC)
- ARP uses broadcast messages, NDP uses multicast ICMPv6 messages
- Both are insecure by default
- NDP can use SNDP, a security extension to NDP to prevent NA/NS attacks

[Unicast, multicast, anycast](https://serverfault.com/a/227488)
- Unicast: host to host
- Multicast: host to group
- Anycast: host to any member of a group

[NDP attacks and mitigations](https://sysadminland.com/neighbor-discovery-attacks-threats-and-mitigation-strategies-for-ipv6-networks/)

[IPv6 Toolkit](https://www.kali.org/tools/ipv6toolkit/) (IPv6 security analysis/exploitation tools)