# Build an image and tag it (working dir Dockerfile):
`docker build --tag name:tag .`

# Build a specific Dockerfile with tag
`docker build --tag name:tag --file /path/to/Dockerfile`

# Build a docker image from a Dockerfile at a specified URL
`docker build --tag jessfrazaptfile:latest https://github.com/jessfraz/dockerfiles/raw/8ff991327fd1c68369c2fb576215d0eabffca0e1/apt-file/Dockerfile`