# Summary
The docker daemon runs as root (by default). Running services in a container as the root user is a bad idea security-wise -- it makes privilege escalation easier and more impactful when a container is compromised. It's better to create and run as an unprivileged user in your containers. Better still, layer this with a rootless container engine like [podman](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md) or [Docker's rootless mode](https://docs.docker.com/engine/security/rootless/)).

Running as an unprivileged user can create filesystem ownership/permission issues -- especially with volume mounts. The snippet below shows how to work around this in a `Dockerfile`.

# Unprivileged user with volume
Replace variables as needed for your application.

```dockerfile
# Unprivileged user (arbitrary UID + GID unlikely to be in use)
ARG CONTAINER_USER=user
ARG CONTAINER_USER_UID=2000
ARG CONTAINER_USER_GID=2000

# Directory paths
ARG APP_SRC_DIR=/home/${CONTAINER_USER}/app
ARG APP_DATA_DIR=${APP_SRC_DIR}/app_data

# Create unprivileged user
RUN groupadd --gid ${CONTAINER_USER_GID} ${CONTAINER_USER} && \
    useradd --create-home --uid ${CONTAINER_USER_UID} --gid ${CONTAINER_USER_GID} ${CONTAINER_USER}

# Set proper ownership for unprivileged user to read/write its files
RUN mkdir ${APP_SRC_DIR} && \
    mkdir ${APP_DATA_DIR} && \
    chown -R ${CONTAINER_USER_UID}:${CONTAINER_USER_GID} ${APP_SRC_DIR} && \
    chown -R ${CONTAINER_USER_UID}:${CONTAINER_USER_GID} ${APP_DATA_DIR}

# Create a volume for logs (manage this via docker-compose)
VOLUME ${APP_DATA_DIR}
```