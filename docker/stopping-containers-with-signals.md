# Run a container indefinitely
In the `Dockerfile` CMD instruction or `docker-compose.yml` command: `tail -f /etc/profile`

# Handling container stop via SIGTERM, SIGKILL, etc
For details see https://kmg.group/posts/2022-05-23-docker-stop-containers-with-signals/

For long or indefinitely running containers, a SIGKILL may be needed. By convention, container runtimes send SIGTERM, and most containers handle SIGTERM to the container's ENTRYPOINT process (typically PID 1 in the container)


## Quick-and-dirty way
Use SIGKILL instead of the default SIGTERM.

### In a Dockerfile

```Dockerfile
from php:8.0-fpm

COPY . /

#--- override the SIGQUIT used in php:8.0-fpm
STOPSIGNAL SIGKILL
```

### Via a docker/podman argument
In the `docker run` or `podman run` command, pass argument `--stop-signal SIGKILL`

### Via a docker-compose.yml
In a `docker-compose.yml`, use the `stop_signal: SIGKILL` directive within a service. For example:
```yml
services:
  long-running-container:
    stop_signal: SIGKILL
    build:
      context: .
```


## Graceful, more involved way
Write a script that traps SIGTERM, close any resources the container is using (files, network connections, whatnot), and exits cleanly.