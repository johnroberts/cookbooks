# Summary
Jekyll is a popular, robust static site generator, written in Ruby.

# Concepts
- Outputs HTML/JS/CSS, making it dead simple to host
- Liquid: the templating language used by Jekyll (similar to jinja2 used by Ansible)
- Page: most basic content building block. Can be `.html` or `.md`. Can be organized into subfolders.
- Posts: dated content in `_posts/YEAR-MONTH-DAY-title.md`
- Front matter: YAML-formatted block at the top of a file, defines metadata such as title, layout to use, etc.
- Collections: a way to group related content (instruments in an ensemble, members of a team, talks at a conference, etc.). Defined in 
- Data: variables usable in liquid templates, defined as YAML/JSON/CSV/TSV files in `_data`. Useful for structured data.
- Static file: a file without front matter, such as images, PDFs, mp3s, whatnot. Typically stored in `assets/`

# Resources
- Solid default Jekyll theme: Chirpy ([getting started guide](https://chirpy.cotes.page/posts/getting-started/), [source code](https://github.com/cotes2020/jekyll-theme-chirpy))
- [Quickstart](https://jekyllrb.com/docs/)
- [Site structure](https://jekyllrb.com/docs/structure/)