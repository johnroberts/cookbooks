# Commands
`cd jekyll-site` before running commands.

Jekyll expects a Jekyll-structured directory (with `_config.yml`, `_posts`, `_tabs`, etc).

## Building/serving
Build a site: `jekyll build` (renders to `./_site` by default)

Build a site (including drafts): `jekyll build --drafts`

Serve a site: `jekyll serve` (defaults to `0.0.0.0:4000` + auto-regeneration)

## Writing/publishing
Create a new post: `jekyll post POST-NAME`

Create a new draft post: `jekyll draft POST-NAME`

Publish a draft post: `jekyll publish POST-NAME`

Unpublish a post: `jekyll unpublish POST-NAME`

## Maintenance
Update [Chirpy static assets](https://github.com/cotes2020/chirpy-static-assets) (self-host JS and fonts, also depends on values in `_config.yml`): `git submodule update`