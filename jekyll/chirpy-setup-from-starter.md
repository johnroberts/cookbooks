# How to set up my blog from Chirpy Jekyll Theme Starter
## Steps
1) Clone https://github.com/cotes2020/chirpy-starter to a new repo
2) Update `Gemfile` with `jekyll-compose` Gem
3) Update `config.yml` and `_tabs/about.md` with personal details (name, tagline, GitHub link, avatar image path, description, etc)
4) Enable [self-hosting static assets](https://github.com/cotes2020/chirpy-static-assets#readme) (`_config.yml`, `git submodule` commands, and update `.github/workflows/pages-deploy.yml` to use submodules)
5) Update footer: `_data/locales/en.yml` -> `meta` var, and `_includes/footer.html`
6) Add/import content
