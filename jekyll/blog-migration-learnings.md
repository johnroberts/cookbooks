# General
- You can override theme defaults by making a copy of a theme's file under `_layouts` or `_includes`. This is used to override the footer.
- Chirpy starter's default Gemfile doesn't include `jekyll-compose`, which is needed to use `jekyll post`, `jekyll draft`, etc. Easy fix, add this line to Gemfile: `gem 'jekyll-compose', group: [:jekyll_plugins]`
- Create an `assets/` directory at a Jekyll project root for static assets like images/audio/documents, then link to them as needed in posts/pages (the rendered versions live under `_site/assets/whatever`)
- Enabling Disqus loads content from 8 domains, including tracking domains for social login (Facebook, Google, etc.)
- Chirpy allows self-hosting static assets (JS, fonts, etc.) by setting a flag in `_config.yml` and git submodules. Details at https://github.com/cotes2020/chirpy-static-assets

# Favicon
- Using color `orangered` (CSS) or `#FF4500` (hex) works well with light and dark themes
- Favicon generator: https://realfavicongenerator.net/
- Favicon creator based on `font-awesome` icons: https://gauger.io/fonticon/
- Using a face as a favicon doesn't work (too small)

# Chirpy
[Upgrade Chirpy version](https://github.com/cotes2020/jekyll-theme-chirpy/wiki/Upgrade-Guide) by:
1) Check what files have changed in the chirpy starter by comparing the previous version to the latest version (https://github.com/cotes2020/chirpy-starter/compare/v5.6.0...v5.6.1)
2) Bumping the version number in the `Gemfile`
3) Rebuilding the site using the `jekyll/jekyll` container (which automatically downloads latest Gem versions upon execution).
