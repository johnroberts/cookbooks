# Run a playbook against localhost
```sh
# Install prereqs
`sudo apt install -y ansible git`

# Run the playbook to set up localhost
ansible-playbook -K --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 playbook.yml
```