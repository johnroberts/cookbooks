# Usage
1. Install `ansible` and `podman` on your localhost
2. Adapt/extend [`playbook-to-build-container.yml`](./playbook-to-build-container.yml) for your needs
3. Run the playbook: `ansible-playbook playbook-to-build-container.yml`
4. Push the resulting image to a registry, run it, deploy it...

# How it works
1. Start a named container from a minimal base image that includes Python
2. Run tasks/roles against the named container
3. Commit the named container to a new image

# Benefits
Building container images with Ansible offer benefits over `Dockerfile`-based builds:
- Declarative build (vs. imperative Dockerfile-based build)
- Composable: combine and re-use existing roles or collections
- Manage full container lifecycle from playbook (pull/build/push/deploy)
- Produce an efficient image (squashing build layers into a single image)

# Requirements
## Host
- podman
- ansible
- [`containers.podman` collection](https://docs.ansible.com/ansible/latest/collections/containers/podman/index.html0) (included with ansible, not ansible-core)

## Build target base image
Choose a base image with a Python interpreter to run Ansible modules. Some good choices:
- `python:latest` (Debian-based, with commonly-used packages installed)
- `python:slim` (Debian-based with minimal packages installed)
- `python:alpine` (Alpine-based)
