# Encrypt/decrypt a file, prompting for password
```sh
ansible-vault encrypt a-secret-file
ansible-vault decrypt a-secret-file
```

# Encrypt/decrypt a file using a password file (useful for automation)
```sh
# Prepend space to keep password out of shell history
 echo 'randomly-generated-safely-stored-password' > vault_pw_file

# Tighten permissions
chmod 400 vault_pw_file

# Encrypt a plaintext file
ansible-vault encrypt --vault-password-file vault_pw_file vars/secret-vars.yml

# Decrypt an encrypted file
ansible-vault decrypt --vault-password-file vault_pw_file vars/secret-vars.yml
```

# Encrypt/decrypt a sensitive file in-place
```shell
# Echo the password to a file
 echo password > password.txt

PASSWORD_FILE=password.txt
FILE=sensitive-file # e.g. project-secret-vars.yml, an SSH or API key, file containing a full connection string

# Encrypt the file in-place
ansible-vault encrypt --vault-password-file $PASSWORD_FILE --output $FILE $FILE

# Decrypt the file in-place
ansible-vault decrypt --vault-password-file $PASSWORD_FILE --output $FILE $FILE
```

# Using ansible-vault with Vagrant
- Supply password via a password file at `provision/vault_pass.txt` (exclude it via `.gitignore`)
- Use `ansible_local` provisioner in `Vagrantfile`, with a playbook that uses encrypted+committed content:
```
config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "playbook-referencing-encrypted-content.yml"
    ansible.vault_password_file = "/provision/vault_pass.txt"
end
```
