# Summary
Boilerplate project: use `ansible` to build and manage a `podman`-based container infrastructure.

# Usage
1) Install `ansible` and `podman`.
2) Edit [vars.yml](vars.yml) to control the container building process (base image, image name/tags, etc.)
3) Edit [playbook-build-container.yml](playbook-build-container.yml) to run whatever tasks/roles you like against the container.
4) Build the container: `ansible-playbook playbook-build-container.yml`

## Common Commands Cheatsheet
### Install/update Ansible roles/collections to the repo
`./scripts/install-deps-to-repo.sh`

### Encrypt & decrypt sensitive files
You can edit this script, adding new entries to the `SECRET_FILEPATHS` variable (relative to project root).

```bash
# Encrypt
./scripts/manage-secret-files.sh encrypt

# Decrypt
./scripts/manage-secret-files.sh decrypt
```