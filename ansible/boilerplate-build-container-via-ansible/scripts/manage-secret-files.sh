#!/usr/bin/env bash

##### Purpose #####
# This script uses ansible-vault to encrypt/decrypt the project's sensitive files.
#   - By default, vars.yml is encrypted/decrypted (in project root)
# 
##### Usage ######
# Designed to be executed from the project's root directory:
#   - scripts/manage-secret-files.sh encrypt
#   - scripts/manage-secret-files.sh decrypt
#
##### Adding Sensitive Content ####
#   1) Generate and securely store a password. Place it as a single line in .vault_pass
#   2) Add the files you want to encrypt to the SECRET_FILEPATHS variable
#   3) Run this script (from the project root) once they contain sensitive values.
#   4) Commit the encrypted secrets to the repository.
# 
##################

# Define password file location 
PASSWORD_FILE=${PASSWORD_FILE:-".vault_pass"}

# Store secret filepaths in an array
# Add your own files as needed:
#   - as a relative path to the working dir when script is executed (intended to be repo root)
#   - as an absolute path
declare -A SECRET_FILEPATHS
SECRET_FILEPATHS["vars"]="vars.yml"

###### Main Logic #####
# Check that the password file exists
if [ ! -f $PASSWORD_FILE ]; then
    echo "Unable to find password file: $PASSWORD_FILE, quitting."
    exit 1
fi

# Check if we're encrypting or decrypting
ACTION=$1

if [ "$ACTION" = "encrypt" ]; then
  # Loop through all sensitive files
  for key in ${!SECRET_FILEPATHS[@]}
  do
    # Encrypt each sensitive file (or warn if it wasn't found)
    if [ -f "${SECRET_FILEPATHS[${key}]}" ]; then
      echo "Encrypting ${SECRET_FILEPATHS[${key}]}..."
      ansible-vault encrypt --vault-password-file $PASSWORD_FILE --output ${SECRET_FILEPATHS[${key}]} ${SECRET_FILEPATHS[${key}]}
    else 
      echo "WARNING: couldn't find secret file ${SECRET_FILEPATHS[${key}]}, skipping"
    fi
  done

  echo "Secrets have been encrypted using the password file $PASSWORD_FILE"

elif [ "$ACTION" = "decrypt" ]; then
  # Loop through all sensitive files
  for key in ${!SECRET_FILEPATHS[@]}
  do
    # Decrypt each sensitive files (or warn if it wasn't found)
    if [ -f "${SECRET_FILEPATHS[${key}]}" ]; then
      echo "Decrypting ${SECRET_FILEPATHS[${key}]}..."
      ansible-vault decrypt --vault-password-file $PASSWORD_FILE --output ${SECRET_FILEPATHS[${key}]} ${SECRET_FILEPATHS[${key}]}
    else 
      echo "WARNING: couldn't find secret file ${SECRET_FILEPATHS[${key}]}, skipping"
    fi
  done

  echo "Secrets have been decrypted using the password file $PASSWORD_FILE"

else
  echo "Unrecognized action $ACTION."
  echo "Run this script as either:"
  echo "  ./scripts/manage-secret-files.sh encrypt"
  echo "  ./scripts/manage-secret-files.sh decrypt"
  exit 1
fi