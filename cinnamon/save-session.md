# Save Cinnamon session at logout (windows/workspace positions)
```
sudo apt install -y dconf-editor
dconf-editor &

# Search org.cinnamon.SessionManager -> auto-save-session as true
```

More at https://unix.stackexchange.com/questions/314749/linux-mint-18-with-cinnamon-session-save-and-restore