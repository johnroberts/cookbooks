# Summary
Rootless podman performs some magic, mapping UIDs and GIDs inside and outside the container to make things work. Sometimes this causes filesystem permission issues. Use `podman unshare` to get a shell in the user namespace shared by podman rootless containers -- this is sometimes needed to change file ownership within that namespace.


## Example
`docker.io/jekyll/jekyll` has a `jekyll` user with UID 1000 inside the container. Using this image, you usually mount a local directory (with the content Jekyll expects) into the container using `-v`. However inside the container, this appears to be owned by root, which means the `jekyll` user with UID 1000 can't write to these files. To fix this, use `podman unshare`:
```bash
# List files in the same user namespace that podman containers share
podman unshare ls -la .

# Change file ownership to the UID of the in-container user
# In this case, jekyll/jekyll image jekyll user is 1000
podman unshare chown 1000:1000 -R .
```

# Reference
https://www.tutorialworks.com/podman-rootless-volumes/