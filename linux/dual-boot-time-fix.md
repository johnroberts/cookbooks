# Summary
When dual-booting Windows and Linux, both set the system clock (main displayed-in-OS clock) to the hardware clock. Linux assumes the hardware clock is in UTC by default, whereas Windows assumes a local-time hardware clock. Desynced clocks are annoying (and mess with logging, certificates, ...)

# Fix
Linux is more flexible than Windows, so have Linux assume hardware clock is in local fime: `timedatectl set-local-rtc 1 --adjust-system-clock`