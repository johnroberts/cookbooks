# In Progress
- [ ] Import snippets from Evernote + Onenote
- [ ] publishing
- [ ] nmap
- [ ] Add `docker-compose.yml.j2` example

# Topic ideas
- Gitlab CI
- AWS CLI
- AWS offensive security
- GCloud API/CLI
- Azurecloud API/CLI
- Serverless
- Serverless security
- OWASP Top 10
- Python
- Ansible
- Linux commands
- SSH