# ~/.ssh/authorized_keys
The `~/.ssh/authorized_keys` contains the public keys that are authorized to log in as a given user.

This file can be created/edited manually, or you can use: `ssh-copy-id -i ~/.ssh/id_ed25519 user@host`