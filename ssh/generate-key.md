# Generate an SSH key (store at ~/.ssh/id_ed25519)
For ED25519: `ssh-keygen -t ed25519 -C "email@example.com"`
For RSA: `ssh-keygen -o -t rsa -b 4096 -C "email@example.com"`

Add a passphrase when prompted.

# Generate an SSH key (specify output location)
For ED25519: `ssh-keygen -t ed25519 -f ~/.ssh/filename -C "email@example.com"`
For RSA: `ssh-keygen -o -t rsa -b 4096 -f ~/.ssh/filename -C "email@example.com"`

Add a passphrase when prompted.

# Key types
See https://docs.gitlab.com/ee/user/ssh.html#ed25519-ssh-keys

Goals: security, compatability

Use:
- ed25519
- RSA > =2048 bits (choose 4096 bits unless authenticating to super low-spec systems)

Avoid:
- DSA
- RSA smaller than 2048 bits