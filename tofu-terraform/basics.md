# Core Concepts
[OpenTofu](https://opentofu.org/) is a Terraform-compatible, fully open-source fork of [Terraform](https://www.terraform.io/).
- Declarative cloud provider configuration/management/automation
- TF excels at managing resources exposed by cloud provider APIs
    - Ansible (or similar) is better for managing resources *within* cloud infrastructure that aren't directly controlled by an API (e.g. instances)
- Prefer OpenTofu to Terraform (to avoid Business Source License)
- Uses HCL: Hashicorp configuration language
- All `.tf` files in a directory are combined into a single configuration

In this document, TF refers to Terraform and OpenTofu interchangeably. It's written primarily with OpenTofu in mind, as OpenTofu is the future of Terraform after the [BSL changeover](https://www.hashicorp.com/blog/hashicorp-adopts-business-source-license).

## Dir Structure
Each module is a directory. Root module can invoke/include submodules.

```bash
tree example-tf-project

.
├── main.tf
├── modules
│   ├── example-resource
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   └── variables.tf
├── outputs.tf
├── provider.tf
├── terraform.tfstate
├── terraform.tfstate.backup
├── terraform.tfvars
└── variables.tf
```

Each module needs:
- `main.tf`: resources
- `variables.tf`: input variables
- `outputs.tf`: output variables

Root module is like other modules, with a few twists:
- `provider.tf`: tell TF how to interact with a cloud service provider API
- `terraform.tfvars`: project-specific values for root module `variables.tf`. It's useful to template this out from Ansible.
- root module `output.tf` displays values when running `tofu apply`, `tofu refresh`, `tofu show`, etc.

## Modules
[Modules](https://developer.hashicorp.com/terraform/language/modules): objects wrapping multiple resources that are used together to achieve some desired result
- Each dir is a module (including root dir/module)
- Execution starts from the root module

## Providers
[Providers](https://developer.hashicorp.com/terraform/language/providers): wrappers around cloud service APIs.
- Some written by Hashicorp, some written by the cloud provider, some by the community
- Automatically installed from a `provider` block (usually `provider.tf` in root module) by running `tofu init`
- Different providers need different configuration (API keys, region info, ...)
- There are providers for [many cloud services](https://registry.terraform.io/browse/providers)

Specify a block defining required providers in the root-module `main.tf`. Then, define the provider's configuration in a root-module `provider.tf`. For example:
```hcl
# In root module main.tf
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# In root module provider.tf
provider "aws" {
  region = "us-east-1"
  access_key = "your_access_key"
  secret_key = "your_secret_key"
}
```

## State
[State management](https://developer.hashicorp.com/terraform/language/state): TF tracks state in a `terraform.tfstate` file (sensitive!).
- Storage location is flexible: local filesystem, some object storage, Terraform Cloud (SaaS-ified Terraform for enterprises)
- State locking is possible (useful when working in teams and/or with CI/CD)
- There's also a `terraform.tfstate.backup` file

## Resource
[Resource](https://developer.hashicorp.com/terraform/language/resources): object representing something TF manages.
- Examples: instance, network, API key, user, object storage bucket, cloud database, a local file...
- Look up docs per-resource on [Terraform Registry](https://registry.terraform.io/)

## Data Source
[Data source](https://developer.hashicorp.com/terraform/language/data-sources): object representing some externally-managed data needed by TF. Think the results of some API call, reading a local file...

## Variables
[Variables](https://developer.hashicorp.com/terraform/language/values): way to pass data to resources. See [Variable Passing](#variable-passing) for details and examples.

# Common Commands
```bash
# Initialize providers, run basic checks of a configuration
tofu init

# Show current resources and state
tofu show

# Detailed configuration check, determine what actions to take, so deployed infra matches TF configuration
tofu plan

# Execute actions to make deployed infra match TF configuration
tofu apply

# Update state against deployed infrastructure
tofu refresh

# Delete TF-managed deployed infrastructure
tofu destroy
```

# Variable Passing
- Variables need to be defined in the root module, and child module.
- Use `var.var_name` to access this module's input variables.
- Use `module.module_name.output_var` to access a child module's output variables from the root module.

## Input Vars
Each module's inputs are defined in `variables.tf`:
```hcl
variable "username" {
    description = "name for some user account"
    type        = string
    default     = "svc_example"
}
```

The root module's outputs are printed on the CLI from commands like `tofu apply`/`tofu show`/etc.

## Output Vars
Each module's outputs are defined in `outputs.tf`, referencing an attribute from the resource's `main.tf`:
```hcl
# In modules/resource_name/main.tf
resource "resource_type" "resource_name" {
    # variable definitions
}

# In modules/resource_name/outputs.tf
output "some-computed-info" {
    value = resource_type.resource_name.attribute
}
```

## Examples
### Pass from root module to child module
Use root module to pass input variables from `terraform.tfvars` (or CLI) to a child module.

```hcl
# In root terraform.tfvars
some_var_in_root_module     = "project-specific-value"

# In root variables.tf
variable "some_var_in_root_module" {
  description = "describe what some_var_in_root_module does"
  type        = string
  default     = "foo" # optional, overrideable in terraform.tfvars
}

# In root main.tf
module "proj_resource_name" {
  source                    = "./modules/resource_name"
  var_name_in_child_module  = var.some_var_in_root_module
}

# In modules/resource_name/variables.tf
variable "var_name_in_child_module" {
  description = "describe what this var does"
  type        = string
  default     = "foo" # optional
}
```

### Pass one module's output to another's input
```hcl
# In root main.tf
module "resource_giving_output" {
  source                    = "./modules/resource_giving_output"
}

module "resource_taking_input" {
  source                    = "./modules/resource_taking_output"
  input_from_another_module = module.resource_giving_output.var_from_output
}

# In modules/resource_taking_input/variables.tf
variable "input_from_another_module" {
    description             = "what this variable does"
    type                    = string
}

# In modules/resource_giving_output/main.tf
resource "resource_name_in_provider" "friendly_name" {
    # variable definitions for this resource
}

# In modules/resource_giving_output/output.tf
output "var_from_output" {
  value = resource_name_in_provider.friendly_name.attribute_from_resource
}
```
