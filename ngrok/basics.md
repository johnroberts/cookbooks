# Summary
ngrok: "reverse proxy as a service"

CLI tool that lets you expose a port from a local host to the internet -- useful for showing a prototype webapp/API to someone or interacting with it from another tool or machine.

# Installation
```bash
curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc
    | tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main"
    | tee /etc/apt/sources.list.d/ngrok.list
    && apt-get update && apt-get install ngrok
```

# Setup (add token)
In a browser:
- Log into ngrok: https://dashboard.ngrok.com/login
- Copy authtoken from https://dashboard.ngrok.com/get-started/your-authtoken

On the CLI with `ngrok` installed: `ngrok config add-authtoken the_copied_authtoken`

# Usage
(On a CLI with `ngrok` installed and authentication)

Expose port 8080 to the Internet: `ngrok http http://localhost:8080`