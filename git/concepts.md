Reference: https://danielmiessler.com/study/git/

# Working directory
A directory with contents you're using git to manage

# Commit/ref
Snapshot of all git-tracked working directory content at a specific point in time.

# Index/staging area
A git-managed place in between working and a commit for "staging" files (`git add`)

# Repository

# Remote

# HEAD

# Branch
A grouped series of commits. 

# .git directory
The `.git` directory (created by `git init`) contains the metadata and data for a repository.