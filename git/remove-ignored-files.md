# Stop tracking an already-committed file that's now .gitignored
`git rm --cached filename`

# Stop tracking all commited files that are now .gitignored
```shell
git rm -r --cached .
git add .
git commit -m 'removed files 
```