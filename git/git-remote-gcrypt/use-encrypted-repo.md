# Summary
1. Import one of the GPG keypairs trusted by the encrypted remote
2. Set git to use the trusted GPG keypair
3. Clone repo

# Step-by-step (HTTPS remote)
This was tested with a Gitlab HTTPS-based remote.

```shell
##### GPG setup #####
# Assumes GPG keys is not in keyring already, key backups are in files:
#   - examplekey.gpg.pub
#   - examplekey.gpg.priv

# Import GPG keys to keyring
gpg --import examplekey.gpg.pub examplekey.gpg.priv

# Determine key fingerprint to use for later commands
gpg --list-keys # note the key fingerprint for later copy/paste

##### Git setup #####
# Tell git to use the now-imported key
# git-remote-gcrypt uses this (or per-repo config with remote.<name>.gcrypt-signingkey)
git config --global user.signingkey "key-fingerprint"

# Clone the encrypted remote (including the branch where an encrypted repo is)
git clone -b main "gcrypt::https://gitlab.com/user/repo.git#main" repo
cd repo

# Optionally expose public key IDs in the ciphertext on the remote
# This avoids multiple credential prompts when interacting with the remote
git config remote.origin.gcrypt-publish-participants true

# Pull from origin, setting it as the upstream
git pull --set-upstream origin main

# From this point, use the repo normally
```

# Step-by-step (SSH remote)
This was tested with a Bitbucket SSH-based remote.

```shell
##### GPG setup #####
# Assumes GPG keys is not in keyring already, key backups are in files:
#   - examplekey.gpg.pub
#   - examplekey.gpg.priv

# Import GPG keys to keyring
gpg --import examplekey.gpg.pub examplekey.gpg.priv

# Determine key fingerprint to use for later commands
gpg --list-keys # note the key fingerprint for later copy/paste

##### Git setup #####
# Tell git to use the now-imported key
# git-remote-gcrypt uses this (or per-repo config with remote.<name>.gcrypt-signingkey)
git config --global user.signingkey "key-fingerprint"

# Clone the encrypted remote (including the branch where an encrypted repo is)
git clone -b main "gcrypt::git@bitbucket.org:user/gcrypt-example.git" repo

cd repo

# Optionally expose public key IDs in the ciphertext on the remote
# This avoids multiple credential prompts when interacting with the remote
git config remote.origin.gcrypt-publish-participants true

# Pull from origin, setting it as the upstream
git pull --set-upstream origin main

# From this point, use the repo normally
```