# Summary
1. Set up prereqs
2. Decide a repository you want to encrypt
3. Create a new encrypted remote by prefixing a typical remote string with `gcrypt::`
4. Specify which GPG keypairs can use (encrypt/decrypt/sign) the remote

# Sample gcrypt remote strings
```shell
# HTTPS-based (tested with Gitlab; should work on Github or other Git hosts)
git remote add origin gcrypt::https://gitlab.com/user/repo.git#main

# Git-based (tested with Bitbucket)
git remote add origin gcrypt::git@bitbucket.org:user/repo.git
```

# Step-by-step (HTTPS remote)
These steps were tested against Gitlab remote.

Make sure `git` and `git-remote-gcrypt` are installed, then:
```shell
##### GPG setup #####
# Generate fresh GPG keypair
gpg --full-generate-key

# Back up your keys! Losing the key = losing the encrypted repo
gpg --export --export-options backup --output ~/public.gpg someone@example.com

# Back up private key to private.gpg
gpg --export-secret-keys --export-options backup --output ~/private.gpg someone@example.com

###### Repo setup #####
# Create a new local repo
git init new-repo
cd new-repo

# Commit some content
echo "this will become an encrypted readme" > README.md
git add -A && git commit -m 'initial commit'

# Create a project/repository on a remote (GitHub/Gitlab/etc.)

# Add a crypt remote, prepending gcrypt:: to the normal remote string
#   - Use HTTPS remotes with GitHub and GitLab
#   - Append branch name to the end of the string
#   - Each push is a force-push; configure this branch as non-protected to allow pushes
git remote add origin gcrypt::https://gitlab.com/user/repo.git#main

# Optionally expose public key IDs in the ciphertext on the remote
# This avoids multiple credential prompts when interacting with the remote
git config remote.origin.gcrypt-publish-participants true

# Get the fingerprints of the GPG keys who should be able to encrypt/decrypt the remote
gpg --list-keys

# Specify GPG keys of repo collaborators (one or multiple)...
git config remote.origin.gcrypt-participants "key-fingerprint"
# Alternately use the below to disable signature checking + encrypt to default GPG key
# git config remote.origin.gcrypt-participants "simple"

# Tell git which GPG keypair to use for encryption/decryption (and signing)
git config --global user.signingkey "key-fingerprint"

# Push the gcrypt remote
git push -u origin main
```

# Step-by-step (SSH remote)
These steps are tested on a BitBucket private repository.

Make sure `git` and `git-remote-gcrypt` are installed, then:
```shell
##### GPG setup #####
# Generate fresh GPG keypair
gpg --full-generate-key

# Back up your keys! Losing the key = losing the encrypted repo
gpg --export --export-options backup --output ~/public.gpg someone@example.com

# Back up private key to private.gpg
gpg --export-secret-keys --export-options backup --output ~/private.gpg someone@example.com

###### Repo setup #####
# Create a new local repo
git init new-repo
cd new-repo

# Commit some content
echo "this will become an encrypted readme" > README.md
git add -A && git commit -m 'initial commit'

# Create a project/repository on Bitbucket

# Add a crypt remote, prepending gcrypt:: to the git-based remote string
git remote add origin gcrypt::git@bitbucket.org:user/gcrypt-example.git

# Optionally expose public key IDs in the ciphertext on the remote
# This avoids multiple credential prompts when interacting with the remote
git config remote.origin.gcrypt-publish-participants true

# Get the fingerprints of the GPG keys who should be able to encrypt/decrypt the remote
gpg --list-keys

# Specify GPG keys of repo collaborators (one or multiple)...
git config remote.origin.gcrypt-participants "key-fingerprint"
# Alternately use the below to disable signature checking + encrypt to default GPG key
# git config remote.origin.gcrypt-participants "simple"

# Tell git which GPG keypair to use for encryption/decryption (and signing)
git config --global user.signingkey "key-fingerprint"

# Push the gcrypt remote
git push -u origin main
```