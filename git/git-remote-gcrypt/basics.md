# Summary
A git remote helper that manages remote encryption/decryption using GPG

# Installation
## Via OS package manager
`sudo apt install -y git-remote-gcrypt`

(many distros have it in default repos)

## Via install script (untested)
```
git clone https://github.com/spwhitton/git-remote-gcrypt.git
./git-remote-gcrypt/install.sh
```

# Prerequisites
## Packages
[ ] `git` is installed
[ ] `git-remote-gcrypt` is installed

## GPG keys
[ ] You have a GPG keypair
[ ] (For multi-person projects) You have the GPG public keys of all collaborators