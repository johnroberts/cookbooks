# Summary
git-remote-gcrypt is a bit picky/wonky, be aware of these in day-to-day usage

# Tips
- Prefer Bitbucket SSH-based remotes; adding keys to the SSH agent with `ssh-add` makes this a better experience than being prompted continually for HTTPS username/password
- Use HTTPS-based remotes for Github/Gitlab; git-based remotes use the rsync backend, which doesn't work with Github/Gitlab
- SSH-based remotes work with Bitbucket, but not with Gitlab or Github
- `git-remote-gcrypt` creates its own branch on the remote always named `master`
- Multiple-PW-prompt-at-push workaround: `git config remote.origin.gcrypt-publish-participants true`. This slightly degrades security; by default `git-remote-gcrypt` uses `gpg -R` to obscure GPG key IDs. Good for use with privately-managed pubkeys.
- Single-person encrypted remotes, disable signature checking: omit `gcrypt.participations` config (equivalent to `remote.origin.gcrypt.participants simple`). This encrypts to default GPG key, and accepts any valid GPG signature.
- `git-remote-gcrypt` wants pushes to `master` on the remote even when local branch is named differently. TODO: override HTTPS remote branch by concatenating `#next` to URL?
- `git-remote-gcrypt` will push to master by default, rather than honoring local branch name and creating this on the server (like standard git commands); to work around this, specify the branch for a gcrypt remote by concatenating `#branch-name` to the end of the remote string. Ex:
```shell
git remote add origin https://gitlab.com/user/repo.git#main
```
- `git-remote-gcrypt` uses --force-push on every commit, which breaks protected branches (which have force push disabled). To work around this, allow force push access (under Repository Settings -> Protected Branches for Gitlab)

# Resources
- README: https://github.com/spwhitton/git-remote-gcrypt
- How-to video: https://www.youtube.com/watch?v=XdoTca3EQGU