# Specify files/dirs to encrypt
.gitattributes:
```
# Encrypt a specific file
secretfile filter=git-crypt diff=git-crypt

# Encrypt all files in a dir
secretdir/** filter=git-crypt diff=git-crypt

# Encrypt all .key files
*.key filter=git-crypt diff=git-crypt
```

# Initialize git-crypt in a repo
Specify files in-scope for `git-crypt` via `.gitattributes` BEFORE adding files.

Then:
```shell
cd repo
git-crypt init
```

# Unlock encrypted files in a repository
## Symmetric encryption
`git-crypt export-key /path/to/keyfile`

## Symmetric decryption
`git-crypt unlock /path/to/keyfile`