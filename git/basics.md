# Set git username and email
```
git config --global user.name "Some Person"
git config --global user.email "someperson@example.com"
```

# Initial branch name to main
`git config --global init.defaultBranch main`

# Commiting
- Commit early and often
- Have descriptive commit messages

## Fast-commit all modified files in current dir
```
git commit -a -m 'commit message'
```

## Selectively commit files
```
git add <files>
git commit -m 'commit message`
```

# Stashes
## Stash modified, tracked files
`git stash push`

## Stash all files (including untracked)
`git stash push -u`

## List stashes
`git stash list`

## Pop a stash (apply to working dir)
`git stash pop {{optional_stash_name}}`

## Drop/discard a specific stash
`git stash drop {{stash_name}}`

## Drop/discard all stashes
`git stash clear`