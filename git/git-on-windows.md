# Configuration (Win10/Win11)
- Git for Windows, managed via Chocolatey
- Windows-native OpenSSH implementation (https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_overview)

With this setup, you can use `git` from the command-line, VSCode, etc. 

# Installing git for Windows, using native SSH
At git for Windows install time, avoid installing the git-for-Windows OpenSSH implementation; instead, it will look in `PATH` and use the native OpenSSH implementation.

First tell chocolatey to remember parameters on upgrade: `choco feature enable -n=useRememberedArgumentsForUpgrades`

Then install git without SSH: `choco install -y git.install --params /NoOpenSSH`

Telling git to use external/native OpenSSH lets `ssh-add` work properly. More info: https://stackoverflow.com/questions/34638462/using-git-with-ssh-agent-on-windows/71190226#71190226