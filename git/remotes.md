# Add a remote
`git remote add <remote-name> <remote-url>`

# Set upstream branch
`git push -u origin main` 

This tells git which remote to use as the upstream for a local branch. This allows future `git pull`/`git push`/`git fetch` and similar commands to work properly.

# Remote types
SSH remotes: `git@gitlab.com:username/repo.git`
HTTPS remotes: `https://gitlab.com/username/repo.git`

Prefer SSH remotes over HTTPS remotes for easier commiting + authentication; prefer HTTPS remotes over SSH remotes for easier cloning.